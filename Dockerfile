FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y

# install java
RUN apt install openjdk-11-jdk -y
RUN java -version

# install Maven
RUN apt -yq install maven --assume-yes

#install curl
RUN apt -yq install curl

#install node
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt -yq install nodejs
RUN node --version

# install npm (should be installed with node)
# RUN apt -yq install npm
# RUN npm --version

# install python3
RUN apt -yq install python3-minimal
RUN python3 --version

# install pip
RUN apt -yq install python3-pip
RUN pip3 --version

# install aws cli
RUN pip3 install awscli
RUN aws --version

#install jq
RUN apt -yq install jq

#install git
RUN apt -yq install git
RUN git --version

#install xmlstarlet
run apt -yq install xmlstarlet
RUN xmlstarlet --version

#install moreutils
RUN apt -yq install moreutils
